(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[11],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/create.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/landing/create.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var _record, _ref;

    return _ref = {
      record: (_record = {
        description: "",
        detalle: "",
        titulo: "",
        price: "",
        //  media: "",
        ubicacion: "",
        telefono: "",
        pago: "",
        pago_is: false,
        dni_is: false,
        direccion_is: false
      }, _defineProperty(_record, "dni_is", false), _defineProperty(_record, "correo_notificacion", ""), _defineProperty(_record, "color", ""), _defineProperty(_record, "colorcabezera", ""), _defineProperty(_record, "colorformulario", ""), _defineProperty(_record, "tama\xF1oletra", ""), _defineProperty(_record, "image", ""), _defineProperty(_record, "image_fondo", ""), _defineProperty(_record, "titulo_formulario", ""), _defineProperty(_record, "promesa", ""), _defineProperty(_record, "tipomedia_id", ""), _defineProperty(_record, "tipomoneda_id", ""), _defineProperty(_record, "image_fondo_id", ""), _defineProperty(_record, "dni", ""), _defineProperty(_record, "direccion", ""), _defineProperty(_record, "colorletra", ""), _defineProperty(_record, "file_id", ""), _defineProperty(_record, "video_url", ""), _record),
      listTipomedia: [],
      fullscreenLoading: false,
      form: new FormData(),
      error: 0,
      listTipomoneda: []
    }, _defineProperty(_ref, "fullscreenLoading", false), _defineProperty(_ref, "error", 0), _ref;
  },
  watch: {
    "record.pago_is": {
      handler: function handler(val) {
        this.record.pago = "";
        this.record.dni = "";
        this.record.direccion = "";
      }
    }
  },
  mounted: function mounted() {
    this.getListarTipomedia();
    this.getListarTipomoneda();
  },
  // mounted() {
  /// this.getListarClientes();
  // },
  methods: {
    limpiarCriterios: function limpiarCriterios() {
      this.record.description = "";
      this.record.detalle = "";
      this.record.titulo = "";
      this.record.titulo_formulario = "";
      this.record.colorformulario = "";
      this.record.promesa = "";
      this.record.price = ""; //  this.record.media = "";
      // this.record.tipo_media = "";
      // this.record.media = "";
      //  this.record.moneda = "";

      this.record.ubicacion = "";
      this.record.telefono = "";
      this.record.pago = "";
      this.record.correo_notificacion = "";
      this.record.image_fondo = "";
      this.record.color = "";
      this.record.colorcabezera = "";
      this.record.dni = "";
      this.record.direccion = "";
      this.record.colorletra = "";
      this.record.tamañoletra = "";
      this.record.tipomedia_id = "";
      this.record.tipomoneda_id = ""; // this.record.client_id = "";
    },
    getFile: function getFile(e) {
      this.record.image = e.target.files[0];
    },
    getFileFondo: function getFileFondo(e) {
      this.record.image_fondo = e.target.files[0];
    },
    //async getListarClientes() {
    //this.fullscreenLoading = true;
    //var url = "/operacion/cliente/index";
    //const res = await this.callApi("get", url);
    //if (res.status == 200) {
    //this.listClientes = res.data;
    // } else {
    // if (res.status == 401) {
    // this.$router.push({ name: "login" });
    //location.reload();
    //localStorage.clear();
    //  }
    //if (res.status == 419) {
    //this.notification("error", "Fue necesario recargar la página", 0);
    //location.reload();
    // }
    // }
    //this.fullscreenLoading = false;
    // },
    //traer imagen
    // getFile(e) {
    // this.record.image = e.target.files[0];
    // },
    //async setRegistrarProducto() {
    // if (this.validarRegistrarProducto()) {
    // return;
    // }
    //this.fullscreenLoading = true;
    //var url = "/landing/landing-page/store";
    //const res = await this.callApi("post", url, this.record);
    //if (res.status == 200) {
    //this.notification("success", "Registro creado con éxito", 0);
    //this.$router.push("/landing-page");
    //} else {
    //if (res.status == 422) {
    //for (let i in res.data.errors) {
    //this.notification("error", res.data.errors[i][0]);
    //}
    // }
    //if (res.status == 401) {
    // this.$router.push({ name: "login" });
    //location.reload();
    //localStorage.clear();
    // }
    //if (res.status == 419) {
    // this.notification("error", "Fue necesario recargar la página", 0);
    //location.reload();
    //}
    //}
    //this.fullscreenLoading = false;
    //},
    getListarTipomedia: function getListarTipomedia() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var url, res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.fullscreenLoading = true;
                url = "/configuracion/tipomedia/index";
                _context.next = 4;
                return _this.callApi("get", url);

              case 4:
                res = _context.sent;

                if (res.status == 200) {
                  _this.listTipomedia = res.data;
                } else {
                  if (res.status == 401) {
                    _this.$router.push({
                      name: "login"
                    });

                    location.reload();
                    localStorage.clear();
                  }

                  if (res.status == 419) {
                    _this.notification("error", "Fue necesario recargar la página", 0);

                    location.reload();
                  }
                }

                _this.fullscreenLoading = false;

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    getListarTipomoneda: function getListarTipomoneda() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var url, res;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.fullscreenLoading = true;
                url = "/configuracion/tipomoneda/index";
                _context2.next = 4;
                return _this2.callApi("get", url);

              case 4:
                res = _context2.sent;

                if (res.status == 200) {
                  _this2.listTipomoneda = res.data;
                } else {
                  if (res.status == 401) {
                    _this2.$router.push({
                      name: "login"
                    });

                    location.reload();
                    localStorage.clear();
                  }

                  if (res.status == 419) {
                    _this2.notification("error", "Fue necesario recargar la página", 0);

                    location.reload();
                  }
                }

                _this2.fullscreenLoading = false;

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    //async setRegistrarProducto(){
    //if (this.validarRegistrarProducto()) {
    //  return;
    // }
    //this.fullscreenLoading = true;
    //if (!this.record.image || this.record.image == undefined) {
    // this.setGuardarUsuario();
    // } else {
    // this.setRegistrarArchivo();
    // }
    //         var url = '/landing/landing-page/store'
    //       const res = await this.callApi('post', url, this.record)
    //     if (res.status == 200) {
    //       this.notification('success', 'Registro creado con éxito', 0)
    //     this.$router.push('/landing-page"');
    //} else {
    //  if (res.status == 422) {
    //    for(let i in res.data.errors){
    //      this.notification('error', res.data.errors[i][0])
    //}
    //}
    //if (res.status == 401) {
    //  this.$router.push({name: 'login'})
    //location.reload();
    //localStorage.clear();
    // }
    //if (res.status == 419) {
    //  this.notification('error', 'Fue necesario recargar la página', 0)
    //location.reload();
    // }
    // }
    //this.fullscreenLoading  =   false;
    // },
    setRegistrarProducto: function setRegistrarProducto() {
      if (this.validarRegistrarProducto()) {
        return;
      }

      this.fullscreenLoading = true;

      if ((!this.record.image || this.record.image == undefined) && (!this.record.image_fondo || this.record.image_fondo == undefined)) {
        this.setGuardarProducto();
      } else {
        if (!this.record.image || this.record.image == undefined) {
          this.setRegistrarArchivoFondo();
        } else {
          this.setRegistrarArchivo();
        }
      }
    },
    setRegistrarArchivo: function setRegistrarArchivo() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var config, url, res, i;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this3.form.append("file", _this3.record.image);

                _this3.form.append("path", "user");

                config = {
                  headers: {
                    "Content-Type": "multipart/form-data"
                  }
                };
                url = "/file/store";
                _context3.next = 6;
                return _this3.callApi("post", url, _this3.form, config);

              case 6:
                res = _context3.sent;

                if (res.status == 200) {
                  _this3.record.file_id = res.data;

                  if (!_this3.record.image_fondo) {
                    _this3.setGuardarProducto();
                  } else {
                    _this3.setRegistrarArchivoFondo();
                  }
                } else {
                  if (res.status == 422) {
                    for (i in res.data.errors) {
                      _this3.notification("error", res.data.errors[i][0]);
                    }
                  }

                  if (res.status == 401) {
                    _this3.$router.push({
                      name: "login"
                    });

                    location.reload();
                    localStorage.clear();
                  }

                  if (res.status == 419) {
                    _this3.notification("error", "Fue necesario recargar la página", 0);

                    location.reload();
                  }
                }

                _this3.fullscreenLoading = false;

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    setRegistrarArchivoFondo: function setRegistrarArchivoFondo() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var config, url, res, i;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this4.form.append("file", _this4.record.image_fondo);

                _this4.form.append("path", "user");

                config = {
                  headers: {
                    "Content-Type": "multipart/form-data"
                  }
                };
                url = "/file/store";
                _context4.next = 6;
                return _this4.callApi("post", url, _this4.form, config);

              case 6:
                res = _context4.sent;

                if (res.status == 200) {
                  _this4.record.image_fondo_id = res.data;

                  _this4.setGuardarProducto();
                } else {
                  if (res.status == 422) {
                    for (i in res.data.errors) {
                      _this4.notification("error", res.data.errors[i][0]);
                    }
                  }

                  if (res.status == 401) {
                    _this4.$router.push({
                      name: "login"
                    });

                    location.reload();
                    localStorage.clear();
                  }

                  if (res.status == 419) {
                    _this4.notification("error", "Fue necesario recargar la página", 0);

                    location.reload();
                  }
                }

                _this4.fullscreenLoading = false;

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    setGuardarProducto: function setGuardarProducto() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var url, res, i;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                url = "/landing/landing-page/store";
                _context5.next = 3;
                return _this5.callApi("post", url, _this5.record);

              case 3:
                res = _context5.sent;

                if (res.status == 200) {
                  _this5.notification("success", "Registro creado con éxito", 0);

                  _this5.$router.push("/landing-page");
                } else {
                  if (res.status == 422) {
                    for (i in res.data.errors) {
                      _this5.notification("error", res.data.errors[i][0]);
                    }
                  }

                  if (res.status == 401) {
                    _this5.$router.push({
                      name: "login"
                    });

                    location.reload();
                    localStorage.clear();
                  }

                  if (res.status == 419) {
                    _this5.notification("error", "Fue necesario recargar la página", 0);

                    location.reload();
                  }
                }

                _this5.fullscreenLoading = false;

              case 6:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    validarRegistrarProducto: function validarRegistrarProducto() {
      this.error = 0;

      if (!this.record.description) {
        this.notification("error", "El nombre es un campo obligatorio", 0);
        this.error = 1;
      }

      if (!this.record.detalle) {
        this.notification("error", "El detalle es un campo obligatorio", 0);
        this.error = 1;
      }

      if (!this.record.telefono) {
        this.notification("error", "El telefono es un campo obligatorio", 0);
        this.error = 1;
      }

      if (!this.record.correo_notificacion) {
        this.notification("error", "El correo es un campo obligatorio", 0);
        this.error = 1;
      }

      if (!this.record.tipomedia_id) {
        this.notification("error", "Debe seleccionar un Tipo", 10);
        this.error = 1;
      }

      if (!this.record.tipomoneda_id) {
        this.notification("error", "Debe seleccionar un Tipo", 10);
        this.error = 1;
      } //  if (!this.record.client_id) {
      //  this.notification("error", "Debe seleccionar un nombre", 10);
      //this.error = 1;
      // }
      // return this.error;

    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/create.vue?vue&type=template&id=1d7fa4ff&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/landing/create.vue?vue&type=template&id=1d7fa4ff& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "pcoded-content" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("div", { staticClass: "page-block" }, [
        _c("div", { staticClass: "row align-items-center" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("ul", { staticClass: "breadcrumb" }, [
              _c(
                "li",
                { staticClass: "breadcrumb-item" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "landing.page" } } },
                    [_c("i", { staticClass: "fas fa-users" })]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "breadcrumb-item" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: { name: "producto.crear" } } },
                    [_vm._v("\n                Crear Producto\n              ")]
                  )
                ],
                1
              )
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-sm-12" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-header" }, [
            _c("div", { staticClass: "col-sm-12" }, [
              _c("div", { staticClass: "btn-actions" }, [
                _c(
                  "div",
                  { staticClass: "btn-group card-option" },
                  [
                    _c(
                      "el-tooltip",
                      {
                        staticClass: "item",
                        attrs: {
                          effect: "dark",
                          content: "Regresar",
                          placement: "top"
                        }
                      },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-icon btn-outline-primary",
                            on: {
                              click: function($event) {
                                $event.preventDefault()
                                return _vm.goBack($event)
                              }
                            }
                          },
                          [_c("i", { staticClass: "fas fa-arrow-left" })]
                        )
                      ]
                    )
                  ],
                  1
                )
              ])
            ]),
            _vm._v(" "),
            _c("h4", [_vm._v("DATOS GENERALES")])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "Titulo" } }, [
                        _vm._v("Titulo General (*)")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.titulo,
                            expression: "record.titulo"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "Titulo",
                          placeholder: "Titulo"
                        },
                        domProps: { value: _vm.record.titulo },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.setRegistrarProducto($event)
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.record, "titulo", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "Nombre" } }, [
                        _vm._v("Nombre (*)")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.description,
                            expression: "record.description"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "Nombre",
                          placeholder: "Nombre"
                        },
                        domProps: { value: _vm.record.description },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.setRegistrarProducto($event)
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.record,
                              "description",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "Detalle" } }, [
                        _vm._v("Descripción (*)")
                      ]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.detalle,
                            expression: "record.detalle"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "Detalle",
                          placeholder: "Detalle"
                        },
                        domProps: { value: _vm.record.detalle },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.setRegistrarProducto($event)
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.record, "detalle", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "Promesa" } }, [
                        _vm._v("Promesa (*)")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.promesa,
                            expression: "record.promesa"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: {
                          type: "text",
                          id: "Promesa",
                          placeholder: "Promesa"
                        },
                        domProps: { value: _vm.record.promesa },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.setRegistrarProducto($event)
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.record, "promesa", $event.target.value)
                          }
                        }
                      })
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", { attrs: { for: "Tipomedia" } }, [
                          _vm._v("Elegir imagen o video (*)")
                        ]),
                        _vm._v(" "),
                        _c(
                          "el-select",
                          {
                            attrs: { placeholder: "Tipomedia", filterable: "" },
                            model: {
                              value: _vm.record.tipomedia_id,
                              callback: function($$v) {
                                _vm.$set(_vm.record, "tipomedia_id", $$v)
                              },
                              expression: "record.tipomedia_id"
                            }
                          },
                          _vm._l(_vm.listTipomedia, function(item) {
                            return _c("el-option", {
                              key: item.id,
                              attrs: { label: item.name, value: item.id }
                            })
                          }),
                          1
                        )
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-sm-6" },
                  [
                    _vm.record.tipomedia_id == 1
                      ? [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { attrs: { for: "Archivo" } }, [
                              _vm._v("Archivo")
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              staticClass: "form-control",
                              class: _vm.record.media ? " image-selected" : "",
                              attrs: { type: "file" },
                              on: { change: _vm.getFile }
                            })
                          ])
                        ]
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.record.tipomedia_id == 2
                      ? [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", { attrs: { for: "Nombre" } }, [
                              _vm._v("URL del Video(*)")
                            ]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.record.video_url,
                                  expression: "record.video_url"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                id: "Urlvideo",
                                placeholder: "URL Video"
                              },
                              domProps: { value: _vm.record.video_url },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    _vm.record,
                                    "video_url",
                                    $event.target.value
                                  )
                                }
                              }
                            })
                          ])
                        ]
                      : _vm._e()
                  ],
                  2
                ),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c(
                    "div",
                    { staticClass: "form-group" },
                    [
                      _c("label", { attrs: { for: "Tipomoneda" } }, [
                        _vm._v("Tipo de moneda (*)")
                      ]),
                      _vm._v(" "),
                      _c(
                        "el-select",
                        {
                          attrs: { placeholder: "Tipomoneda", filterable: "" },
                          model: {
                            value: _vm.record.tipomoneda_id,
                            callback: function($$v) {
                              _vm.$set(_vm.record, "tipomoneda_id", $$v)
                            },
                            expression: "record.tipomoneda_id"
                          }
                        },
                        _vm._l(_vm.listTipomoneda, function(item) {
                          return _c("el-option", {
                            key: item.id,
                            attrs: { label: item.name, value: item.id }
                          })
                        }),
                        1
                      )
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Ubicacion" } }, [
                      _vm._v("Dirección (*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.ubicacion,
                          expression: "record.ubicacion"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Ubicacion",
                        placeholder: "Ubicacion"
                      },
                      domProps: { value: _vm.record.ubicacion },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.record, "ubicacion", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Telefono" } }, [
                      _vm._v("Telefono (*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.telefono,
                          expression: "record.telefono"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Telefono",
                        placeholder: "Telefono"
                      },
                      domProps: { value: _vm.record.telefono },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.record, "telefono", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Pago" } }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.pago_is,
                            expression: "record.pago_is"
                          }
                        ],
                        staticClass: "mr-2",
                        attrs: { type: "checkbox" },
                        domProps: {
                          checked: Array.isArray(_vm.record.pago_is)
                            ? _vm._i(_vm.record.pago_is, null) > -1
                            : _vm.record.pago_is
                        },
                        on: {
                          change: function($event) {
                            var $$a = _vm.record.pago_is,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = null,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.record,
                                    "pago_is",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.record,
                                    "pago_is",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.record, "pago_is", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v("Pago (*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.pago,
                          expression: "record.pago"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Pago",
                        placeholder: "Pago",
                        disabled: !_vm.record.pago_is
                      },
                      domProps: { value: _vm.record.pago },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.record, "pago", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Correo_notificacion" } }, [
                      _vm._v("Correo_notificacion(*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.correo_notificacion,
                          expression: "record.correo_notificacion"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Correo_notificacion",
                        placeholder: "Correo_notificacion"
                      },
                      domProps: { value: _vm.record.correo_notificacion },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.record,
                            "correo_notificacion",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("h4", [_vm._v("PERSONALIZACIÓN DE LADINGPAGE")]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "" } }, [
                      _vm._v("Elegir imagen de fondo")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control",
                      class: _vm.record.image_fondo_id ? " image-selected" : "",
                      attrs: { type: "file" },
                      on: { change: _vm.getFileFondo }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Color" } }, [
                      _vm._v("Color Fondo(*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      attrs: { type: "hidden", value: "rpt", id: "idcolor" }
                    }),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.color,
                          expression: "record.color"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "color",
                        id: "Color",
                        placeholder: "Color"
                      },
                      domProps: { value: _vm.record.color },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.record, "color", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Colorletra" } }, [
                      _vm._v("Color Letra(*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.colorletra,
                          expression: "record.colorletra"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "color",
                        id: "Colorletra",
                        placeholder: "Colorletra"
                      },
                      domProps: { value: _vm.record.colorletra },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.record,
                            "colorletra",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "colorcabezera" } }, [
                      _vm._v("Color Cabezera(*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.colorcabezera,
                          expression: "record.colorcabezera"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "color",
                        id: "Colorcabezera",
                        placeholder: "Colorcabezera"
                      },
                      domProps: { value: _vm.record.colorcabezera },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.record,
                            "colorcabezera",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Titulo_formulario" } }, [
                      _vm._v("Titulo Formulario (*)")
                    ]),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.titulo_formulario,
                          expression: "record.titulo_formulario"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Titulo_formulario",
                        placeholder: "Titulo_formulario"
                      },
                      domProps: { value: _vm.record.titulo_formulario },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.record,
                            "titulo_formulario",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "colorformulario" } }, [
                      _vm._v("Color Furmulario(*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.colorformulario,
                          expression: "record.colorformulario"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "color",
                        id: "Colorformulario",
                        placeholder: "Colorformulario"
                      },
                      domProps: { value: _vm.record.colorformulario },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.record,
                            "colorformulario",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c("h4", [_vm._v("AGREGAR CAMPOS AL FORMULARIO OPCIONAL")]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Dni" } }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.dni_is,
                            expression: "record.dni_is"
                          }
                        ],
                        staticClass: "mr-2",
                        attrs: { type: "checkbox" },
                        domProps: {
                          checked: Array.isArray(_vm.record.dni_is)
                            ? _vm._i(_vm.record.dni_is, null) > -1
                            : _vm.record.dni_is
                        },
                        on: {
                          change: function($event) {
                            var $$a = _vm.record.dni_is,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = null,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.record,
                                    "dni_is",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.record,
                                    "dni_is",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.record, "dni_is", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v("Dni (*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.dni,
                          expression: "record.dni"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Dni",
                        placeholder: "Dni",
                        disabled: !_vm.record.dni_is
                      },
                      domProps: { value: _vm.record.dni },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.record, "dni", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("label", { attrs: { for: "Direccion" } }, [
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.record.direccion_is,
                            expression: "record.direccion_is"
                          }
                        ],
                        staticClass: "mr-2",
                        attrs: { type: "checkbox" },
                        domProps: {
                          checked: Array.isArray(_vm.record.direccion_is)
                            ? _vm._i(_vm.record.direccion_is, null) > -1
                            : _vm.record.direccion_is
                        },
                        on: {
                          change: function($event) {
                            var $$a = _vm.record.direccion_is,
                              $$el = $event.target,
                              $$c = $$el.checked ? true : false
                            if (Array.isArray($$a)) {
                              var $$v = null,
                                $$i = _vm._i($$a, $$v)
                              if ($$el.checked) {
                                $$i < 0 &&
                                  _vm.$set(
                                    _vm.record,
                                    "direccion_is",
                                    $$a.concat([$$v])
                                  )
                              } else {
                                $$i > -1 &&
                                  _vm.$set(
                                    _vm.record,
                                    "direccion_is",
                                    $$a.slice(0, $$i).concat($$a.slice($$i + 1))
                                  )
                              }
                            } else {
                              _vm.$set(_vm.record, "direccion_is", $$c)
                            }
                          }
                        }
                      }),
                      _vm._v("Dirección (*)")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.record.direccion,
                          expression: "record.direccion"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "Direccion",
                        placeholder: "Direccion",
                        disabled: !_vm.record.direccion_is
                      },
                      domProps: { value: _vm.record.direccion },
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.setRegistrarProducto($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(_vm.record, "direccion", $event.target.value)
                        }
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-sm-12" }, [
                    _c(
                      "button",
                      {
                        directives: [
                          {
                            name: "loading",
                            rawName: "v-loading.fullscreen.lock",
                            value: _vm.fullscreenLoading,
                            expression: "fullscreenLoading",
                            modifiers: { fullscreen: true, lock: true }
                          }
                        ],
                        staticClass: "btn btn-outline-primary has-ripple",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.setRegistrarProducto($event)
                          }
                        }
                      },
                      [_vm._v("\n                  Guardar\n                ")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-outline-info has-ripple",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            return _vm.limpiarCriterios($event)
                          }
                        }
                      },
                      [_vm._v("\n                  Limpiar\n                ")]
                    )
                  ])
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "page-header-title" }, [
      _c("h5", { staticClass: "m-b-10" }, [_vm._v("Gestión Productos")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-sm-6" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("label", { attrs: { for: "Tamañoletra" } }, [
          _vm._v("Tamaño de letra(*)")
        ]),
        _vm._v(" "),
        _c(
          "select",
          {
            staticClass: "form-select",
            attrs: { "aria-label": "Default select example" }
          },
          [
            _c("option", { attrs: { selected: "" } }, [
              _vm._v("Open this select menu")
            ]),
            _vm._v(" "),
            _c("option", { attrs: { value: "13" } }, [_vm._v("13")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "14" } }, [_vm._v("14")]),
            _vm._v(" "),
            _c("option", { attrs: { value: "15" } }, [_vm._v("15")])
          ]
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/landing/create.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/landing/create.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _create_vue_vue_type_template_id_1d7fa4ff___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create.vue?vue&type=template&id=1d7fa4ff& */ "./resources/js/components/landing/create.vue?vue&type=template&id=1d7fa4ff&");
/* harmony import */ var _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create.vue?vue&type=script&lang=js& */ "./resources/js/components/landing/create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _create_vue_vue_type_template_id_1d7fa4ff___WEBPACK_IMPORTED_MODULE_0__["render"],
  _create_vue_vue_type_template_id_1d7fa4ff___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/landing/create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/landing/create.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/landing/create.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/landing/create.vue?vue&type=template&id=1d7fa4ff&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/landing/create.vue?vue&type=template&id=1d7fa4ff& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_1d7fa4ff___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./create.vue?vue&type=template&id=1d7fa4ff& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/landing/create.vue?vue&type=template&id=1d7fa4ff&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_1d7fa4ff___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_1d7fa4ff___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);