<?php

namespace Database\Seeders;

use App\Models\Tipomedia;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TipomediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tipomedia::create([
            'name' => Str::upper('Imagen')
        ]);
        Tipomedia::create([
            'name' => Str::upper('Video')
        ]);
       
    }
}
