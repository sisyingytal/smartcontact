<?php

namespace Database\Seeders;

use App\Models\Tipomoneda;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TipomonedaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tipomoneda::create([
            'name' => Str::upper('Sol')
        ]);
        Tipomoneda::create([
            'name' => Str::upper('Dolar')
        ]);
       
    }
}
