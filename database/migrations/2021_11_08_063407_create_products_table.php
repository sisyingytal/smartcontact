<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
        
            $table->increments('id');
            // $table->unsignedInteger('client_id')->nullable();
             $table->string('description')->nullable();
             $table->string('detalle')->nullable();
             $table->string('titulo')->nullable();
             $table->decimal('price', 8, 2, true)->nullable();
           //  $table->string('tipo_media');
             $table->string('media')->nullable();
            // $table->string('moneda');
             $table->string('ubicacion')->nullable();
             $table->string('telefono')->nullable();
             $table->string('descripcion')->nullable();
             $table->string('pago')->nullable();
             $table->string('correo_notificacion')->nullable();
             $table->string('image_fondo')->nullable();
             $table->string('titulo_formulario')->nullable();
             $table->string('promesa')->nullable();
             $table->string('color')->nullable();
             $table->string('colorcabezera')->nullable();
             $table->string('colorletra')->nullable();
             $table->string('colorformulario')->nullable();
             $table->string('tamañoletra')->nullable();
             $table->string('direccion')->nullable();
             $table->string('telefono_form')->nullable();
             $table->string('dni')->nullable();
             $table->string('dni_is')->nullable();
             $table->string('direccion_is')->nullable();
             $table->string('opcional')->nullable();
            
           
            // $table->unsignedBigInteger('companys_id')->nullable();
         $table->unsignedInteger('company_id')->nullable();
         $table->unsignedInteger('tipomedia_id')->nullable();
         $table->unsignedInteger('tipomoneda_id')->nullable();
         $table->unsignedInteger('file_id')->nullable();
             $table->enum('status', ['A', 'I'])->default('A');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companys');
         $table->foreign('tipomedia_id')->references('id')->on('tipomedia');
         $table->foreign('tipomoneda_id')->references('id')->on('tipomoneda');
         $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
