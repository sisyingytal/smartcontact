<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local', function (Blueprint $table) {
            $table->idlocal();
            $table->string('nombre');
            $table->string('direccion');
            $table->char('iddepartamento', 2);	
            $table->char('idprovincia', 2);	
            $table->char('iddistrito', 2);	
            $table->char('tipo', 1);	
            $table->integer('vacantes');
            $table->string('idugel');
            $table->char('idnivel', 1);
            $table->char('etapa', 1);
            $table->char('oficioinvitacion', 10);
            $table->char('oficiorespuesta', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local');
    }
}
