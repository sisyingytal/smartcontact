<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_respuestas', function (Blueprint $table) {
            $table->id_det_rptas();
            $table->integer('id_respuestas');
            $table->integer('id_pregunta');
            $table->text('respuesta');	    
            $table->integer('id_dimension');
            $table->float('puntaje_obtenido');	
            $table->integer('id_dimension');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_respuestas');
    }
}
