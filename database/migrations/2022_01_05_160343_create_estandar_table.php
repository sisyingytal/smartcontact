<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstandarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estandar', function (Blueprint $table) {
            $table->id_estandar();
            $table->string('nombre');
            $table->integer('id_rubrica');
            $table->text('puntuacion');
            $table->integer('id_dimension');
            $table->tinyInteger('activo');	
            $table->string('tipo');
            $table->integer('duplicado_id_estandar');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estandar');
    }
}
