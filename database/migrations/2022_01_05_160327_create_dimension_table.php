<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDimensionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimension', function (Blueprint $table) {
            $table->id_dimension();
            $table->string('nombre');
            $table->string('tipo_pregunta');
            $table->text('escalas_evaluacion');	
            $table->string('tipo');
            $table->text('otros_datos');	
            $table->integer('id_rubrica');
            $table->tinyInteger('activo');	

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimension');
    }
}
