<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno', function (Blueprint $table) {
            $table->dni();
            $table->string('ape_paterno');
            $table->string('ape_materno');
            $table->string('nombre');
            $table->date('fechanac');
            $table->char('sexo',1);
            $table->char('estado_civil',1);
            $table->char('ubigeo',8);
            $table->string('urbanizacion');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('celular');
            $table->string('email');
            $table->char('idugel',4);
            $table->integer('regusuario');
            $table->date('regfecha');
            $table->string('usuario');
            $table->string('clave');
            $table->string('token');
            $table->string('foto');
            $table->tinyInteger('estado');
            $table->tinyInteger('situacion');
            $table->timestamps();	
	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumno');
    }
}
