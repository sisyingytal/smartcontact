<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form', function (Blueprint $table) {
           
            $table->increments('id');
            // $table->unsignedInteger('client_id')->nullable();
             $table->string('nombres')->nullable();
             $table->string('mail')->nullable();
             $table->string('direction')->nullable();
             $table->string('telephone')->nullable();
             //$table->string('pago')->nullable();
             $table->unsignedInteger('product_id')->nullable();
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');

           
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form');
    }
}
