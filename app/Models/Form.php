<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;
    protected $table="form";

    /**
     * The accessors to append to the model's array form.
     *
     * @var arraystatus_name
     */
    protected $appends = [''];
    
    /**
     * Get the tipo that owns the client.
     */
   

    public function getStatusNameAttribute()
    {
        if ($this->status == 'A') {
            return 'Activo';
        }
        return 'Inactivo';
    }
}