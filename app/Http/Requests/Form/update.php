<?php

namespace App\Http\Requests\Form;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'nombres' => [
                'bail',
                'required',
                Rule::unique('form')->where(function ($query) {
                })->ignore($request->id),
            ],
           
            'mail' => [
                'bail',
                'required'
            ],
            'direction' => [
                'bail',
                'required'
            ],
            'telephone' => [
                'bail',
                'required'
            ],
          //  'pago' => [
            //    'bail',
              //  'required'
            //],
            //'fechapago' => [
              //  'bail',
                //'required'
            //]
        ];
    }

    public function attributes()
    {
        return [
            'nombres' => 'Nombres',
           // 'apellidos' => 'Apellidos',
            'mail' => 'Mail',
            'direction' => 'Direccion',
            'telephone' => 'Teléfono Celular',
          //  'pago' => 'Pago',
            //'fechapago' => 'Fecha de pago',
        ];
    }
}