<?php

namespace App\Http\Controllers;

use App\Http\Requests\Form\store;
use App\Http\Requests\Form\update;
use App\Classes\MPDFClass;
use App\Models\LandingForm;
use App\Models\Form;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class FormController extends Controller
{
    /**
     * Create a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $records = Form::where('nombres', 'like', '%' . $request->filter . '%')
            //->orWhere('apellidos', 'like', '%' . $request->filter . '%')
            ->orWhere('mail', 'like', '%' . $request->filter . '%')
            ->orWhere('direction', 'like', '%' . $request->filter . '%')
            ->orWhere('telephone', 'like', '%' . $request->filter . '%')
            //->orWhere('pago', 'like', '%' . $request->filter . '%')
            //->orWhere('fechapago', 'like', '%' . $request->filter . '%')
            ->orderBy('id', 'desc')
            ->take(1000)
            ->get();

        return response()->json($records);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function onlyActive(Request $request)
    {
        $records = Form::where([
                ['status', 'A'],
                // ['code', 'like', '%' . $request->filter . '%'],
                // ['name', 'like', '%' . $request->filter . '%'],
                ['telephone', 'like', '%' . $request->filter . '%'],
            ])
            ->orderBy('id', 'desc')
            ->skip($request->numMin)
            ->take($request->numMax)
            ->get(['id', 'nombres', 'status', 'telephone']);

        return response()->json($records);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(store $request)
    {
        $record = new Form; 

        $record->nombres = $request->nombres;
        //$record->apellidos = Str::upper($request->apellidos);
        $record->mail = Str::upper($request->mail);
        $record->direction = Str::upper($request->direction);
        $record->telephone = Str::upper($request->telephone);
        //$record->pago = Str::upper($request->pago);
        //$record->fechapago = Str::upper($request->fechapago);
      

        $record->save();
    }

    public function storeMultiple(Request $request)
    {
        try {
            DB::beginTransaction();

            $data = $request->listReportes;

            foreach (array_chunk($data, 1000) as $t) {
                DB::table('form')->insertOrIgnore($t);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = Form::find($id);

        return response()->json($record);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(update $request, $id)
    {
        $record = Form::find($id);

        $record->nombres = $request->nombres;
       // $record->apellidos = Str::upper($request->apellidos);
        $record->mail = Str::upper($request->mail);
        $record->direction = Str::upper($request->direction);
        $record->telephone = $request->telephone;
       // $record->pago = Str::upper($request->pago);
       // $record->fechapago = Str::upper($request->fechapago);
       

        $record->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $record = Form::find($id);

        $record->status = $request->status;

        $record->save();
    }
    public function getURI(Request $request)
    {
        // return URL::full();
        return URL::to('/reports/payments' . $request->id );
    }
    public function LandingForm()
    {
        $reports = LandingForm::get();
        $pdf = new MPDFClass(["margin_top" => 17, "format" => "A4", "title" => "Reporte de consumo mensual por paciente"]);
        $pdf->loadView('reports.LandingForm.report_pdf', compact('reports'));
        return $pdf->stream();
    }
    public function Payments()
    {
        $reports = LandingForm::whereNotNull('pago')->get();
        $pdf = new MPDFClass(["margin_top" => 17, "format" => "A4", "title" => "Reporte de consumo mensual por paciente"]);
        $pdf->loadView('reports.Payments.report_pdf', compact('reports'));
        return $pdf->stream();
    }
}
