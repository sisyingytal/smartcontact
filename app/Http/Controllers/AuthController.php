<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $username   =   $request->username;
        $password   =   $request->password;

        //$id = $request->id;
        // var_dump($username);
        //var_dump($password);
        Log::info($username);

        Log::info($password);



        if (!$token = auth()->attempt(
            [
                'username' => $username,
                'password' => $password,
                'status' => 'A'
            ]
        )) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        //
        $logeo = DB::connection('rubrica')->table('personal')->where('usuario', $username)->first();
        // Log::info("Resultaod del logeo",$logeo);
        //si existe el usuario sisy en la bd de personal
        if (!is_null($logeo)) {
            Log::info("Ingreso aqui al if del null logeo");
            session_id('proyecto1');
            session_start();
            $_SESSION["s_id_usu"] = $logeo->dni;
            $_SESSION["cargo"] = $logeo->rol;
            $_SESSION["nombre"] = $logeo->nombre;
            $_SESSION["ape_paterno"] =$logeo->ape_paterno;

            //Log::info($logeo->dni);
            //Log::info($logeo->rol);
            //Log::info($logeo->nombre);

        } else {

   
            $logeo = DB::connection('mysql')->table('users')->where('username', $username)->first();
            //  var_dump($logeo);

            $id         =   $logeo->id;
            $nombre = $logeo->name;
            $email = $logeo->email;
            $fecha = date('Y-m-d');
            $clave = $username;
            //var_dump($id);

            DB::connection('rubrica')->insert(
                'insert into personal(`dni`,`nombre`, `fechanac`,`email`,`usuario`,`clave`,`rol`) values (?, ?, ?,?,?,?,?)',
                [$id, $nombre, $fecha, $email, $username,$clave,1]
            );
            session_id('proyecto1');
            session_start();

        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        session_id('proyecto1');
        session_start();
        session_destroy();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL()
        ]);
    }
}
