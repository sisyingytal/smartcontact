<?php

namespace App\Http\Controllers;

use App\Http\Requests\SMS\send;
use App\Services\MessageApi;
use App\Services\TestService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class SmsController extends Controller
{
    /**
     * Create a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function send(Request $request)
    {
        try {
            DB::beginTransaction();

            $recipients = $request->recipients;
            $record = $request->record;

            $test = resolve(MessageApi::class);

            foreach (array_chunk($recipients, 1000) as $t) {
                foreach ($t as $key => $value) {

                    $typeMessage = $request->typeMessage;

                    if ($typeMessage == 1) {
                        $body = $record['body'];
                    } else {
                        $body = Str::replaceFirst('{NOMBRE}', $value['name'], $record['bodyDefault']);
                        $body = Str::replaceFirst('{PAGO}', $value['pago'], $record['bodyDefault']);
                    }

                    $nameUser = Str::substr($value['name'], 0, 40);

                    //$message_uuid = $test->sendSimpleSMS($record['from'], $value['phone_number'], $body, $nameUser);
                    // $message_uuid = $test->sendWhatsApp($record['from'], $value['phone_number'], $body, $nameUser);
                    $message_uuid = $test->sendMessage($value['phone_number'], $body);

                    DB::table('sms')->insertOrIgnore([
                        'to' => $value['phone_number'],
                        'body' => $body,
                        'from' => $record['from'],
                        'client_id' => $value['id'],
                        'message_uuid' => ($message_uuid) ? $message_uuid : null,
                        'status' => ($message_uuid) ? 'S' : 'F',
                    ]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
    }
}
